(ns friba.backend
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cljs-http.client :as http]
            [cljs.core.async :refer [<! >! chan]]
            [reagent.session :as session]))

;https://fribamobileapp.azurewebsites.net/api/PlayerDocument
;curl -X GET --header "Accept: application/json" --header "ZUMO-API-VERSION: 2.0.0" "https://fribamobileapp.azurewebsites.net/api/PlayerDocument"

(def base-url "https://fribamobileapp.azurewebsites.net/api/")



(defn http-call [req]
  (let [c (chan)]
    (go
      (let [response (<! (http/request (merge {:method            :get
                                               :headers           {"ZUMO-API-VERSION" "2.0.0"}
                                               :with-credentials? false
                                               :query-params      {"ignore" (random-uuid)}}
                                              req)))]
        (>! c response)))
    c))


(defn new-player [player]
  (http-call {:url         (str base-url "PlayerDocument")
              :method      :post
              :json-params player}))

(defn get-players []
  (http-call {:url (str base-url "PlayerDocument")}))

(defn get-player [id]
  (http-call {:url (str base-url "PlayerDocument/" id)}))

(defn update-player [player]
  (http-call {:url         (str base-url "PlayerDocument/" (:id player))
              :method      :post
              :json-params player}))

(defn delete-player [{:keys [id]}]
  (prn "delete-player " id)
  (http-call {:url    (str base-url "PlayerDocument/" id)
              :method :delete}))


(defn new-game [game]
  (http-call {:url         (str base-url "GameDocument")
              :method      :post
              :json-params game}))

(defn get-games []
  (http-call {:url (str base-url "GameDocument")}))

(defn update-game [game]
  (http-call {:method :post
              :url (str base-url "GameDocument/" (:id game))
              :json-params game}))

(defn delete-game [id]
  (prn "delete-game " id)
  (http-call {:url    (str base-url "GameDocument/" id)
              :method :delete}))
;(.table js/console (clj->js (:body response)))
