(ns friba.core
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [reagent.core :as reagent :refer [atom]]
            [reagent.session :as session]
            [goog.events :as events]
            [goog.history.EventType :as HistoryEventType]
            [secretary.core :as secretary :include-macros true]
            [cljs-http.client :as http]
            [friba.login :as login]
            [friba.backend :as backend]
            [cljs.core.async :refer [<!]])
  (:import goog.History))

(enable-console-print!)

(defn load-players []
  (go
    (let [response (<! (backend/get-players))]
      (session/put! :players (:body response)))))

(defn load-games []
  (go
    (let [games (:body (<! (backend/get-games)))]
      (session/put! :games games))))

(defn create-new-player [player]
  (go
    (let [status (:status (<! (backend/new-player player)))]
      (if (= 201 status)
        (load-players)))))

(defn create-new-game [game]
  (go
    (let [response (<! (backend/new-game game))
          game (:body response)]
      (session/put! :game game))))


(defn progress []
  [:div.progress [:div.indeterminate]])

(defn delete-player [player]
  (go
    (let [response (<! (backend/delete-player player))]
      (load-players))))

(defn delete-game [game-id]
  (go
    (let [response (<! (backend/delete-game game-id))]
      (load-games))))

(defn cell2 [putt]
  [:div.col.s2
   [:p
    [:input {:type "checkbox" :checked (:success putt)}]
    [:label]]
   [:label]])

(defn single-player-result [{game-id :id [game-player] :gamePlayers} game]
  [:div {:class "row"}
   [:div {:class "col s12 m12"}
    [:div {:class "card  teal lighten-2 z-depth-3 hoverable"}
     [:div {:class "card-content"}
      [:span {:class "card-title"} (:name game-player)]
      [:span {:class "card-title right"}
       [:a.secondary-content
        {:href     "#!"
         :on-click (fn [e]
                     (delete-game game-id)
                     (.preventDefault e))}
        [:i.material-icons "delete"]]]
      [:div.container.second-container
       [:div.row
        [:div.col.s1 " "]
        [:div.col.s2 "1"]
        [:div.col.s2 "2"]
        [:div.col.s2 "3"]
        [:div.col.s2 "4"]
        [:div.col.s2 "5"]
        [:div.col.s1 " "]]
       (for [{distance :distance, putts :Putts} (:PuttDistances game-player)]
         ^{:key (random-uuid)} [:div.row
                                [:div.col.s1 (str distance "m")]
                                (for [putt putts]
                                  ^{:key (random-uuid)} [cell2 putt])
                                ])]]]]]
  )
(defn game-view [game]
  (if (= 1 (count (:gamePlayers game)))
    [single-player-result game]
    [:a.collection-item
     [:span {:class "right"}
      [:a
       {:href     "#!"
        :on-click (fn [e]
                    (delete-game (:id game))
                    (.preventDefault e))}
       (:id game)
       [:i.material-icons "delete"]]]])
  )

(defn games-page []
  (let [games (session/get :games)]
    (if games
      [:div.collection
       (for [game games]
         ^{:key (:id game)} [game-view game])]
      [progress])))

(defn player-view [player]
  [:li.collection-item
   [:div
    (:name player)
    [:a.secondary-content
     {:href     "#!"
      :on-click #(delete-player player)}
     [:i.material-icons "delete"]]]])



(defn row-button [title icon attrs]
  [:div.row
   [:div.col.s12.center-align
    [:a.waves-effect.waves-light.btn-large.blue-grey.lighten-1 (merge {} attrs)
     [:i.material-icons.left icon] title]]])

(defn finish-button []
  [row-button "Finish" "done" {:href     "#"
                               :on-click #(backend/update-game (session/get :game))}])

(defn main-menu-page []
  (fn []
    (if (:signed-in? @login/user)
      [:div.container.main-container
       [row-button "Single Player Game" "perm_identity" {:href "#single-player-game"}]
       [row-button "Multiplayer Game" "supervisor_account" {:href "#select-players"}]
       [row-button "Games" "list" {:href "#games"}]
       [row-button "Players" "recent_actors" {:href "#players"}]
       [row-button "High Scores" "star" {:href "#high-scores"}]
       [:div.row {:on-click #(login/sign-out)}
        [:div.col.s12.center-align
         [:a.waves-effect.waves-light.btn-large.red.lighten-3
          [:i.material-icons.left "power_settings_new"] "Sign Out"]]]]
      [:div.container.main-container
       [:div.row {:on-click #(login/sign-in)}
        [:div.col.s12.center-align
         [:a.waves-effect.waves-light.btn-large.blue-grey.lighten-1
          [:i.material-icons.left "power_settings_new"] "Sign in with Google"]]]])))

(defn player-identity-and-score [user score]
  [:div.row
   [:div.col.s11
    [:h5 (str (:name user))]]
   [:div.col.s1
    [:h5 score]]])

(defn cell [row col matrix points-matrix]
  (let [checked (reagent/atom false)]
    (fn []
      [:div.col.s2 {:on-click (fn [e]
                                (swap! checked not)
                                (if @checked
                                  (swap! matrix assoc-in [row col] (get-in points-matrix [row col]))
                                  (swap! matrix assoc-in [row col] 0))
                                (session/update-in! [:game :gamePlayers 0 :puttDistances row :putts col :success] not)
                                (.preventDefault e))}
       [:p {:title (str (get-in points-matrix [row col]) " pistettä")}
        [:input {:type "checkbox" :checked @checked}]
        [:label (if (= 4 col) (apply + (get @matrix row)))]]])))

(defn row [matrix meter row points-matrix]
  (fn []
    (let [value (get @matrix row)]
      [:div.row
       [:div.col.s2 {:dangerouslySetInnerHTML {:__html (str meter "&nbsp;m")}}]
       (for [[col _] (map-indexed vector value)]
         ^{:key (str "col_value_" row "_" col)} [cell row col matrix points-matrix])
       ])))



(defn multiplayer-game-grid []
  (let [matrix (reagent/atom
                 [[0 0 0 0 0]
                  [0 0 0 0 0]
                  [0 0 0 0 0]
                  [0 0 0 0 0]
                  [0 0 0 0 0]])
        meters [2 5 8 11 14]]
    (fn []

      (let [score (reduce + (for [[row meter] (map-indexed vector meters)]
                              (* meter (apply + (get @matrix row)))))]
        [:div.container
         [player-identity-and-score (first (session/get :game-players)) score]
         [:div.row
          [:div.col.s2 " "]
          [:div.col.s2 "1"]
          [:div.col.s2 "2"]
          [:div.col.s2 "3"]
          [:div.col.s2 "4"]
          [:div.col.s2 "5"]
          [:div.col.s1 " "]]
         (for [[index meter] (map-indexed vector meters)]
           ^{:key index} [row matrix meter index])]))))

(defn card [game-grid multi]
  (let [players (vec (session/get :game-players))
        prev-player (last players)
        next-player (second (first (partition 3 (cycle players))))]
    [:div {:class "row"}
     [:div {:class "col s12 m12"}
      [:div {:class "card  teal lighten-2 z-depth-3 hoverable"}
       [:div {:class "card-content"}
        [:span {:class "card-title"} ""]
        [:p game-grid]]
       (if multi
         [:div {:class "card-action"}
          [:a {:on-click #(session/assoc-in! [:game-players] (concat [prev-player] (butlast players)))} (str "<- " (:name prev-player))]
          [:a.right {:on-click #(session/assoc-in! [:game-players] (concat (rest players) [(first players)]))} (str (:name next-player) " ->")]])]]]))

(defn single-player-game-grid [game scores]
  (fn []
    (let [rules (:rules game)
          distances (:distances rules)
          points-matrix (:scoresPerPuts rules)
          total-scores (apply + (flatten @scores))]
      [:div.container.second-container
       [player-identity-and-score (first (:gamePlayers game)) total-scores]
       [:div.row
        [:div.col.s2 {:dangerouslySetInnerHTML {:__html "&nbsp;"}}]
        (for [[index meter] (map-indexed vector distances)]
          ^{:key (str "col_header_" index)} [:div.col.s2 (inc index)])
        ]
       (for [[index meter] (map-indexed vector distances)]
         ^{:key (str "col_value_" index)} [row scores meter index points-matrix])])))



(defn footer []
  [:footer.page-footer.light-blue.darken-1
   [:div.container]
   [:div.footer-copyright.light-blue.darken-2
    [:div.container
     [:div.col.s12.center-align
      "2016 Copyright Rapat"]]]])

(defn input [add-fn]
  [:div.input-field
   [:input {:type         "text"
            :on-key-press (fn [e]
                            (if (= 13 (.-charCode e))
                              (add-fn (-> e .-target .-value))))}]

   [:label "Name"]])


(defn new-player-row [cancel-fn]
  [:li.collection-item.avatar
   [:i.material-icons "account_circle"]
   [:span.title]
   [input (fn [name]
            (create-new-player {:name name})
            (cancel-fn))]

   [:a.secondary-content
    {:href     "#!"
     :on-click cancel-fn}
    [:i.material-icons "delete"]]])

(defn players-page []
  (session/put! :show-add-new-player false)
  (fn []
    (let [players (session/get :players)]
      (if players
        [:div
         (if-not (session/get :show-add-new-player)
           [:a.waves-effect.waves-light.btn-large.blue-grey.lighten-1 {
                                                                       :on-click #(session/update! :show-add-new-player not)}
            [:i.material-icons "add"]])
         [:ul.collection
          (if (session/get :show-add-new-player)
            [new-player-row #(session/update! :show-add-new-player not)])
          (for [p players]
            ^{:key (:id p)} [player-view p])]]
        [progress]))))

(defn select-player [player]
  (if (some #{player} (session/get :game-players))
    (session/update-in! [:game-players] disj player)
    (session/update-in! [:game-players] conj player)))

(defn player-item [player]
  (let [selected (some #{player} (session/get :game-players))
        class (if selected "teal  lighten-2" "")]
    [:li.collection-item {:class class :on-click #(select-player player)} (:name player)]))

(defn select-players-list [players]
  [:ul.collection
   (for [player players]
     ^{:key (:id player)}
     [player-item player])])

(defn start-multiplayer-game-button []
  (let [players-selected? (not-empty (session/get :game-players))]
    [:div.col.s12.center-align
     [:a.waves-effect.waves-light.btn-large.blue-grey.lighten-1 {:class    (if players-selected? "" "disabled")
                                                                 :href     "#multiplayer-game"
                                                                 :on-click (fn [e]
                                                                             (if-not players-selected?
                                                                               (.preventDefault e)))}
      [:i.material-icons.right "send"] (if players-selected? "start" "Select players")]]))

(defn select-players-page []
  (if-let [players (session/get :players)]
    [:div
     [start-multiplayer-game-button]
     [select-players-list players]
     [start-multiplayer-game-button]]
    [progress]))


(defn multiplayer-game-page []
  [:div.container
   [card [multiplayer-game-grid] true]
   [finish-button]])

(defn single-player-game-page []
  (let [game (session/get :game)]
    (if game
      [:div
       [card
        (let [scores (reagent/atom
                       [[0 0 0 0 0]
                        [0 0 0 0 0]
                        [0 0 0 0 0]
                        [0 0 0 0 0]
                        [0 0 0 0 0]])]
          [single-player-game-grid game scores]) false]
       [finish-button]]
      [progress])))

(defn high-scores-page []
  [:ol.collection
   (for [player (session/get :players)]
     ^{:key (:id player)}
     [:li.collection-item (:name player)])])

(defn nav []
  [:div {:class "navbar-fixed"}
   [:nav
    ;[actions]
    [:div {:class "nav-wrapper light-blue darken-2"}
     ;[:img {:src (:image-url @login/user), :alt "", :class "circle responsive-img left"}]
     [:a {:href "#", :class "brand-logo center"} "Friba"]]]])

(defn page []
  [(session/get :page)])
;; -------------------------
;; Routes
(secretary/set-config! :prefix "#")



(secretary/defroute "/" []
                    (session/put! :page main-menu-page))

(secretary/defroute "/single-player-game" []
                    (session/remove! :game)
                    (create-new-game {:gamePlayers [(dissoc @login/user :signed-in? :token :image-url)]})
                    (session/put! :page single-player-game-page))

(secretary/defroute "/select-players" []
                    (session/remove! :players)
                    (session/put! :game-players #{})
                    (load-players)
                    (session/put! :page select-players-page))

(secretary/defroute "/multiplayer-game" []
                    (session/put! :page multiplayer-game-page))

(secretary/defroute "/games" []
                    (session/remove! :games)
                    (load-games)
                    (session/put! :page games-page))

(secretary/defroute "/players" []
                    (session/remove! :players)
                    (load-players)
                    (session/put! :page players-page))

(secretary/defroute "/high-scores" []
                    (session/remove! :players)
                    (load-players)
                    (session/put! :page high-scores-page))


;; -------------------------
;; History
;; must be called after routes have been defined
(defn hook-browser-navigation! []
  (doto (History.)
    (events/listen
      HistoryEventType/NAVIGATE
      (fn [event]
        (secretary/dispatch! (.-token event))))
    (.setEnabled true)))

(defn mount-components []
  (reagent/render [nav] (.getElementById js/document "navbar"))
  (reagent/render [page] (.getElementById js/document "app"))
  (reagent/render [footer] (.getElementById js/document "footer")))


(defn init! []
  (session/put! :page main-menu-page)
  (hook-browser-navigation!)
  (mount-components))

(init!)
(defn on-js-reload [])
;; optionally touch your app-state to force rerendering depending on
;; your application
;; (swap! app-state update-in [:__figwheel_counter] inc)

