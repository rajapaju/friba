(ns friba.login
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require
    [reagent.core :as r]
    [reagent.session :as session]
    [friba.backend :as backend]
    [cljs.core.async :refer [put! chan <! >! buffer]]))


; https://developers.google.com/identity/sign-in/web/reference#gapiauth2googleauth


(enable-console-print!)

(def user (r/atom {}))

(defn- load-gapi-auth2 []
  (let [c (chan)]
    (.load js/gapi "auth2" #(go (>! c true)))
    c))

(defn- auth-instance []
  (.getAuthInstance js/gapi.auth2))

(defn- get-google-token []
  (-> (auth-instance) .-currentUser .get .getAuthResponse .-id_token))

(defn- handle-user-change [u]
  (let [profile (.getBasicProfile u)]
    (go
      (let [response (<! (backend/get-players))
            players (:body response)
            name (if profile (.getName profile) nil)]

        (reset! user
                (merge (first (filter #(= (:name %) name) players))
                       {:name       name
                        :image-url  (if profile (.getImageUrl profile) nil)
                        :token      (get-google-token)
                        :signed-in? (.isSignedIn u)}))
        (session/put! :players players)))
    ))

(defonce _ (go
             (<! (load-gapi-auth2))
             (.init js/gapi.auth2
                    (clj->js {"client_id" "47315704837-g9k0csoq5o68ca6a14d9ropctqd0clim.apps.googleusercontent.com"
                              "scope"     "profile"}))
             (let [current-user (.-currentUser (auth-instance))]
               (.listen current-user handle-user-change))))


(defn sign-out []
  (.signOut (auth-instance)))

(defn sign-in []
  (.signIn (auth-instance)))

(defn signed-in? []
  (:signed-in? @user))

